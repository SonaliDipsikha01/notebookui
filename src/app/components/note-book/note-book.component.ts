import { ThrowStmt } from "@angular/compiler";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  RequiredValidator,
  Validators,
} from "@angular/forms";
import { MatMenuTrigger } from "@angular/material";

@Component({
  selector: "app-note-book",
  templateUrl: "./note-book.component.html",
  styleUrls: ["./note-book.component.less"],
})
export class NoteBookComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  constructor(private _formBuilder: FormBuilder) {}
  create: boolean = false;
  noteBookId = 2;
  createNotePanel: boolean = false;
  checkBoxValue: boolean = false;
  editNote: any;
  noteBookData = [
    {
      id: 1,
      noteTitle: "note1",
      noteContent: "hellow",
      time: "24,march,2016",
      editable: true,
    },
    {
      id: 2,
      noteTitle: "note2",
      noteContent: "hello2",
      time: "24,march,2018",
      editable: false,
    },
  ];
  lastUpdatedNote: any=this.noteBookData[1];
  public noteForm: FormGroup;
  ngOnInit(): void {
    this.intiateFormControlForFilter();
  }

  intiateFormControlForFilter(): void {
    this.noteForm = this._formBuilder.group({
      noteTitle: ["", Validators.required],
      noteContent: ["", Validators.required],
      editable: [""],
    });
  }

  openNoteBookPanel(create = false) {
    this.create = true;
    this.noteForm.reset();
    this.createNotePanel = !this.createNotePanel;
  }

  addNote(formData) {
    formData.value["time"] = new Date();
    this.createNotePanel = false;
    this.create = false;
    if (!this.editNote) {
      formData.value["id"] = ++this.noteBookId;
      this.noteBookData.push(formData.value);
      this.lastUpdatedNote=formData.value;
    } else {
      this.noteBookData.forEach((data, index) => {
        if (this.editNote.id == data.id) {
          formData.value["id"] = this.editNote.id;
          this.noteBookData[index] = formData.value;
          this.lastUpdatedNote=formData.value;
        }
      });
    }
    this.editNote = null;
  }

  get noteFormControl() {
    return this.noteForm.controls;
  }
  
  openEditMode(data) {
    this.createNotePanel = true;
    this.noteForm.reset();
    this.noteFormControl.noteTitle.setValue(data.noteTitle);
    this.noteFormControl.noteContent.setValue(data.noteContent);
    this.noteFormControl.editable.setValue(data.editable);
    this.editNote = data;
  }
}
