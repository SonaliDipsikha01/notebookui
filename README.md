# README #
Note Module

### Required in system ###
1.Node js
2.npm package manager

### How do I get set up? ###
1.clone the repository.
get inside folder NoteBookUI
2.install package.json using 'npm i', so all the dependecy will be available to run the project
3.'npm i' will create node modules folder with all the dependecies

### How do I run the project? ###
Once project set up done with above steps,
run 'ng serve' to build and launch to the server.

Go to 'http://localhost:4200/'

Or can also run the build main.ts file inside dist folder (for eg with python)
